FROM python:3
ENV PROJECT_DIR=/app
RUN mkdir -p /app
ADD hello_etg.py /app/
EXPOSE 8000 
RUN pip install flask
CMD [ "python3", "/app/hello_etg.py"]
